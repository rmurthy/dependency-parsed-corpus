package iitb.ac.in;

import iitb.ac.in.nGramExtractor.Bigram;
import iitb.ac.in.nGramExtractor.nGram;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

public class TreeCorpus 
{
	public static void main(String[] args) 
	{
		if(args.length > 3)
		{
			System.out.println("Usage: treeParsedCorpus");
		}
		
		nGram bigram = new Bigram();
		
		ArrayList<String> validRelations = new ArrayList<>();
		
		readConfiguration(validRelations, args[2]);
		
//		validRelations.add("acomp");
//		validRelations.add("advmod");
//		validRelations.add("agent");
//		validRelations.add("amod");
//		validRelations.add("conj");
//		validRelations.add("dep");
//		validRelations.add("dobj");
//		validRelations.add("iobj");
//		validRelations.add("nsubj");
//		validRelations.add("nsubjpass");
//		validRelations.add("number");
//		validRelations.add("poss");
//		validRelations.add("root");
//		validRelations.add("vmod");
//		validRelations.add("xsubj");
		
		try
		{
			BufferedReader br = new BufferedReader(new FileReader(args[0]));
			
			String line ;
			Boolean readLine = true;
			String[] dependencyRelations;
			String relation;
			String word1;
			String word2;
			int count = 0;
			
			while((line = br.readLine()) != null)
			{
				line = line.trim();
				count++;
				if(line.isEmpty())
				{
					readLine = true;
					continue;
				}
				if(readLine)
				{
					//I am about to read sentence
					readLine = false;
					if(count % 50000 == 0)
					{
						System.out.println("\rRead " + count + " number of lines");
					}
				}
				else
				{
					//In the process to read dependency relations
					dependencyRelations = line.split("\\)"); 
					
					//System.out.println(line);
					
					for(int i = 0; i < dependencyRelations.length ; i++)
					{
						relation = dependencyRelations[i].substring(0, dependencyRelations[i].indexOf("("));
						
						word1 = dependencyRelations[i].substring(dependencyRelations[i].indexOf("(")+1, dependencyRelations[i].indexOf(","));

						word2 = dependencyRelations[i].substring(dependencyRelations[i].indexOf(",")+1);
						
						if(word1 == null || word2 == null || relation == null)
							continue;
						else if(word1.isEmpty() || relation.isEmpty() || word2.isEmpty())
							continue;
						
						if(validRelations.contains(relation))
						{
							if(relation.contentEquals("root"))
							{
								//bigram.addWord(word2, "<VERB>");
							}
//							else if(relation.contentEquals("acomp"))
//							{
//								bigram.addWord(word2, word1);
//								bigram.addWord(word1, word2);
//							}
//							else if(relation.contentEquals("advmod"))
//							{
//								bigram.addWord(word2, word1);
//								bigram.addWord(word1, word2);
//							}
//							else if(relation.contentEquals("agent"))
//							{
//								bigram.addWord(word2, word1);
//								bigram.addWord(word1, word2);
//							}
//							else if(relation.contentEquals("amod"))
//							{
//								bigram.addWord(word2, word1);
//								bigram.addWord(word1, word2);
//							}
//							else if(relation.contentEquals("conj"))
//							{
//								bigram.addWord(word2, word1);
//								bigram.addWord(word1, word2);
//							}
//							else if(relation.contentEquals("dep"))
//							{
//								bigram.addWord(word2, word1);
//								bigram.addWord(word1, word2);
//							}
							else if(relation.contentEquals("dobj"))
							{
								bigram.addWord(word2, word1);
								bigram.addWord(word1, word2);
							}
							else if(relation.contentEquals("iobj"))
							{
								bigram.addWord(word2, word1);
								bigram.addWord(word1, word2);
							}
							else if(relation.contentEquals("nsubj"))
							{
								bigram.addWord(word2, word1);
								bigram.addWord(word1, word2);
							}
							else if(relation.contentEquals("nsubjpass"))
							{
								bigram.addWord(word2, word1);
								bigram.addWord(word1, word2);
							}
//							else if(relation.contentEquals("number"))
//							{
//								bigram.addWord(word2, word1);
//								bigram.addWord(word1, word2);
//							}
//							else if(relation.contentEquals("poss"))
//							{
//								bigram.addWord(word2, word1);
//								bigram.addWord(word1, word2);
//							}
							else if(relation.contentEquals("xsubj"))
							{
								bigram.addWord(word2, word1);
								bigram.addWord(word1, word2);
							}
//							else
//							{
//								bigram.addWord(word1, word2);
//								bigram.addWord(word1, word2);
//							}
						}
					}
					readLine = true;
				}
			}
			br.close();
			System.out.println("Read " + bigram.getSize() + " number of dependency relations");
			
			((Bigram)bigram).saveTree("DRdumpBigramBidirect");
			
			((Bigram)bigram).clear();
			
			//DependencyRelation.main(new String[]{"DRdumpBigramBidirect",args[1] });
		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.exit(-1);
		}
	}
	
	public static void readConfiguration(ArrayList<String> validRelations, String filename)
	{
		try
		{
			BufferedReader br = new BufferedReader(new FileReader(filename));
			String line;
			
			while((line = br.readLine()) != null)
			{
				line = line.trim();
				if(line.isEmpty())
				{
					continue;
				}
				else
				{
					validRelations.add(line);
				}
			}
			br.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.exit(-2);
		}
	}
}
