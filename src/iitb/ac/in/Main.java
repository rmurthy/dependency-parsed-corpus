package iitb.ac.in;

import iitb.ac.in.Utilities.MapUtil;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.trees.*;

public class Main 
{
	public static void main(String[] args) 
	{
		if(args.length != 5)
		{
			System.out.println("Usage: vocabulary inputFile outputFile lineNumber configurationFile");
			System.exit(-1);
		}
		
		Map<String, Integer> vocabulary = new TreeMap<>();
		
		//Create vocabulary with frequency counts
		createVocabularyFile(args[1], vocabulary);
		
		vocabulary.clear();
		
		createVocabulary("vocabularyWikiIndexedTemp",vocabulary, args[0]);
		
		
		int lineNumber = Integer.valueOf(args[3]);
		
		try
		{
			LexicalizedParser lp = LexicalizedParser.loadModel("edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz");
			 
			BufferedReader br = new BufferedReader(new FileReader(args[1]));
			
			BufferedWriter bw = new BufferedWriter(new FileWriter(args[2], true));
			String line = null;
			
			TreebankLanguagePack tlp = new PennTreebankLanguagePack();
		    GrammaticalStructureFactory gsf = tlp.grammaticalStructureFactory();
		    // You could also create a tokenizer here (as below) and pass it
		    // to DocumentPreprocessor
		    
		    int count = 0, index = 1;
			
		    long start = System.currentTimeMillis();
			
			while((line = br.readLine()) != null)
			{
				if(count%10000 == 0)
				{
					long diff = System.currentTimeMillis() - start;
					
					System.out.println("Time taken = " + (diff / 1000) + " seconds");
					start = System.currentTimeMillis();
					
					BufferedWriter bw1 = new BufferedWriter(new FileWriter("temp"));
					bw1.write(line);
					bw1.newLine();
					bw1.write(index);
					bw1.close();
				}
				
				if(lineNumber != 0)
				{
					if(index == lineNumber)
					{
						lineNumber = 0;
					}
					else
					{
						index++;
						continue;
					}
				}
			
				if(line.trim().split(" ").length <= 2)
				{
					count++;
					continue;
				}
				
				index++;
				
				Tree parse = lp.parse(line.toLowerCase());
			    count++;
			    
			    GrammaticalStructure gs = gsf.newGrammaticalStructure(parse);
			    Collection<?> tdl = gs.typedDependenciesCCprocessed();
			    
			    Object[] list = tdl.toArray();
			    
			    TypedDependency typedDependency;
			    
			    bw.write(line);
			    bw.newLine();
			    for(Object object : list)
				{
					typedDependency = (TypedDependency) object;
					
					bw.write(typedDependency.reln().getShortName() + "(" +
					typedDependency.gov().nodeString() + "," + 
							typedDependency.dep().nodeString() + ")");
				}
			    
			    bw.newLine();
			    bw.newLine();
			}
			br.close();
			bw.close();
			
			vocabulary.clear();
			
			TreeCorpus.main(new String[]{args[2], args[4]});
			
			DependencyRelation.main(new String[]{"DRdumpBigramBidirect",args[0] });
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void createVocabularyFile(String filename, Map<String, Integer> vocab)
	{
		long count = 0;
		try
		{
			BufferedReader br = new BufferedReader(new FileReader(filename));
			
			String line = null;
			
			while((line = br.readLine()) != null)
			{
				line = line.trim();
				if(line.isEmpty())
					continue;
				
				line = line.replaceAll("[ \t]+", " ");
				line = line.toLowerCase();
				line = line.replaceAll("[0-9]", "D");
				
				String[] words = line.split("[ \t]");
				
				for(int i = 0; i < words.length; i++)
				{
					if(vocab.containsKey(words[i]))
					{
						vocab.put(words[i], vocab.get(words[i]) + 1);
					}
					else
					{
						vocab.put(words[i], 1);
						count++;
						if(count %1000 == 0)
						{
							System.out.print("\rRead " + count + " number of words");
						}
					}
				}
			}

			br.close();
			
			vocab = MapUtil.sortByValue(vocab);
			
			BufferedWriter bw = new BufferedWriter(new FileWriter("vocabularyWikiIndexedTemp"));
			
			Iterator<?> it = null;

	        it = vocab.entrySet().iterator();

	        while(it.hasNext())
	        {
	        	@SuppressWarnings("rawtypes")
				Map.Entry pairs = (Map.Entry) it.next();

                bw.write((String) pairs.getKey() + "\t" + pairs.getValue() + "\n"); 
            }
            bw.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.exit(-1);
		}
	}
	
	public static void createVocabulary(String filename, Map<String, Integer> vocab, String hashfilename)
	{
		long count = 0;
		try
		{
			BufferedReader br = new BufferedReader(new FileReader(filename));
			
			String line = null;
			int index = 0;
			
			while((line = br.readLine()) != null)
			{
				count++;
				line = line.trim();
				if(line.isEmpty())
					continue;
				
				String[] words = line.split("[ \t]");
				
				if(words.length != 2)
				{
					System.out.println(line + "problem at line " + count);
				}
				
				if(Integer.valueOf(words[1]) < 10)
				{
					if(!vocab.containsKey("<UNK>"))
					{
						vocab.put("<UNK>", index);
						index++;
					}
				}
				else
				{
					if(!vocab.containsKey(words[0]))
					{
						vocab.put(words[0], index);
						index++;
					}
					else
					{
						System.err.println("Duplicate entry " + words[0]);
						System.err.println("Need to debug, problem with code");
					}
				}
			}
			br.close();
			
			BufferedWriter bw = new BufferedWriter(new FileWriter(hashfilename));
			
			Iterator<?> it = null;

	        it = vocab.entrySet().iterator();

	        while(it.hasNext())
	        {
	        	@SuppressWarnings("rawtypes")
				Map.Entry pairs = (Map.Entry) it.next();

                bw.write((String) pairs.getKey() + "\t" + pairs.getValue() + "\n"); 
            }
            bw.close();

		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.exit(-1);
		}
	}
}
