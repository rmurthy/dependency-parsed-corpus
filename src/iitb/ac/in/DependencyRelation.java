package iitb.ac.in;

import iitb.ac.in.nGramExtractor.Bigram;
import iitb.ac.in.nGramExtractor.nGram;
import iitb.ac.in.Utilities.*;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class DependencyRelation 
{
	public static void main(String[] args) 
	{
		if(args.length != 2)
		{
			System.out.println("Usage: bigramDump rowVocabulary ");
			System.exit(-2);
		}
		
		Map<String, Integer> rowVocabulary = new TreeMap<>();
		Map<String, Integer> columnVocabulary = new TreeMap<>();
		ArrayList<String> rootWords = new ArrayList<>();
		
		readVocabulary(args[1], rowVocabulary);
		
		nGram bigram = new Bigram();
		
		try
		{
			BufferedReader br = new BufferedReader(new FileReader(args[0]));
			
			String line = null;
			
			while((line = br.readLine()) != null)
			{
				line = line.trim();
				
				String[] words = line.split("\t");
				
				String[] word = words[0].split(" ");
				
				if(word[1].contentEquals("<VERB>"))
				{
					rootWords.add(word[0]);
					continue;
				}
				
				for(int i = 0 ; i < word.length ; i++)
				{
					word[i] = word[i].replaceAll("[ \t]+", " ");
					word[i] = word[i].toLowerCase();
					word[i] = word[i].replaceAll("[0-9]", "D");
					
					if(columnVocabulary.containsKey(word[i]))
					{
						columnVocabulary.put(word[i], columnVocabulary.get(word[i]) + 1);
					}
					else
					{
						columnVocabulary.put(word[i],1);
					}
				}
				
				if(rowVocabulary.containsKey(word[0]))
				{
					bigram.addWord(word[0], word[1]);
				}
				else
				{
					bigram.addWord("<UNK>", word[1]);
				}
				
				if(rowVocabulary.containsKey(word[1]))
				{
					bigram.addWord(word[1], word[0]);
				}
				else
				{
					bigram.addWord("<UNK>" , word[0]);
				}
			}
			br.close();
			
			System.out.println("Read row vocabulary containing " + rowVocabulary.size() + " and column"
					+ " vocabulary containing " + columnVocabulary.size() + " words");
			
			columnVocabulary = shrinkVocabulary(columnVocabulary);
			
			System.out.println("Read row vocabulary containing " + rowVocabulary.size() + " and column"
					+ " vocabulary containing " + columnVocabulary.size() + " words");
			
			writeVocabulary("DependencyColumnsHashedHeuristic", columnVocabulary);
			
			int count = 0;
			BufferedWriter bw = new BufferedWriter(new FileWriter("cooccurrenceMatrixBidirect"));
			for(String key : rowVocabulary.keySet())
			{
				ArrayList<String> columns = ((Bigram) bigram).getBigram(key);
			
				Boolean flag = false;

				for(int i = 0; i < columns.size(); i++)
				{
					if(columnVocabulary.containsKey(columns.get(i)))
					{
						flag = true;
					}
				}
				
				
				if(flag)
				{
					bw.write(key + " ");
					System.out.println(key);
					count++;
					
					columns = ((Bigram) bigram).getBigram(key);
					
					for(int i = 0; i < columns.size(); i++)
					{
						if(columnVocabulary.containsKey(columns.get(i)))
						{
							bw.write(columnVocabulary.get(columns.get(i)) + ":" + 
									bigram.getCount(key,columns.get(i)) + " ");
						}
					}
					
					if(rootWords.contains(key))
					{
						columns = ((Bigram) bigram).getFirstWord(key);
						
						for(int i = 0; i < columns.size(); i++)
						{
							if(columnVocabulary.containsKey(columns.get(i)))
							{
								bw.write(columnVocabulary.get(columns.get(i)) + ":" + bigram.getCount(key,
										columns.get(i)) + " ");
							}
						}
					}
					bw.newLine();
					bw.flush();
					System.out.println("\r Wrote " + count + " number of lines");
				}
				
			}
			bw.close();
			
			rowVocabulary.clear();
			columnVocabulary.clear();
			((Bigram)bigram).clear();
			
			readFile("cooccurrenceMatrixBidirect");
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.exit(-1);
		}
	}

	static void readFile(String inFile)
	{
		try
		{
			Set<Integer> columns = new HashSet<>();
			
			BufferedReader br = new BufferedReader(new FileReader(inFile));
			
			String line = null;
			
			Map<Integer,WordContext> words = new TreeMap<>();
			int index = 0;
			
			while((line = br.readLine()) != null)
			{
				line = line.trim();
				
				String[] word = line.split(" ");
				if(word.length == 1)
					continue;
				
				WordContext temp = new WordContext();
					
				for(int i = 1; i < word.length; i++)
				{
					columns.add(Integer.valueOf(word[i].split(":")[0]));
					temp.vector.put(Integer.valueOf(word[i].split(":")[0]),
							(double)Integer.valueOf((word[i].split(":")[1])));
				}
				
				temp.word = word[0];
				words.put(index,temp);
				index++;
			}
			br.close();
			
			//standardize(words, columns);
			
			BufferedWriter bw = new BufferedWriter(new FileWriter("temp"));
			
			for (Map.Entry<Integer, WordContext> entry : words.entrySet())
			{
				WordContext t = entry.getValue();
				
				bw.write(t.word + " " );
					
					for (Map.Entry<Integer, Double> entry1 : t.vector.entrySet())
					{
						int index1 = entry1.getKey();
						double value = entry1.getValue();
						
						bw.write(index1 + ":" + value + " ");
					}
					bw.newLine();
					bw.flush();
			}
			bw.close();
			
			columns.clear();
			words.clear();

		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	static void standardize(Map<Integer,WordContext> words, Set<Integer> columns)
	{
		Iterator<Integer> it = columns.iterator();
		int i = 0;
		while(it.hasNext()) 
		{
			int columnIndex = (int) it.next();
			double columnCount = 0;
			
			for(Integer key : words.keySet())
			{
				if(words.get(key).vector.containsKey(columnIndex))
				{
					columnCount += words.get(key).vector.get(columnIndex);
				}
			}
			if(columnCount != 0.0)
				for(Integer key : words.keySet())
				{
					if(words.get(key).vector.containsKey(columnIndex))
					{
						words.get(key).vector.put(columnIndex, words.get(key).vector.get(columnIndex) 
								/ columnCount);
					}
				}
		}
	}
	
	static void standardize(Map<Integer,WordContext> words, Set<Integer> columns, Map<Integer, Double> idf)
	{
		Iterator<Integer> it = columns.iterator();
		int i = 0;
		while(it.hasNext()) 
		{
			int columnIndex = (int) it.next();
			double columnCount = 0;
			
			int noOfContextWord = 0;
			Map<Integer,Double> count = new TreeMap<>();
			
			for(Integer key : words.keySet())
			{
				if(words.get(key).vector.containsKey(columnIndex))
				{
					columnCount += words.get(key).vector.get(columnIndex);
					noOfContextWord++;
				}
			}
			
			if(columnCount != 0.0)
				for(Integer key : words.keySet())
				{
					if(words.get(key).vector.containsKey(columnIndex))
					{
						words.get(key).vector.put(columnIndex, words.get(key).vector.get(columnIndex) 
								/ columnCount);
					}
				}
			
			for(Integer key : words.keySet())
			{
				if(words.get(key).vector.containsKey(columnIndex))
				{
					words.get(key).vector.put(columnIndex, words.get(key).vector.get(columnIndex) 
							* Math.log((words.size() * 1.0) / noOfContextWord));
				}
			}
		}
	}

	
	public static void readVocabulary(String filename, Map<String, Integer> vocabulary)
	{
		long count = 0;
		try
		{
			BufferedReader br = new BufferedReader(new FileReader(filename));
			
			String line = null;
			
			while((line = br.readLine()) != null)
			{
				count++;
				line = line.trim();
				if(line.isEmpty())
					continue;
				
				String[] words = line.split("[ \t]");
				
				if(words.length != 2)
				{
					System.out.println(line + "problem at line " + count);
				}
				
				vocabulary.put(words[0], Integer.valueOf(words[1]));
			}
			br.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.exit(-1);
		}
	}

	public static void writeVocabulary(String filename, Map<String, Integer> vocabulary)
	{
		try
		{
			BufferedWriter bw = new BufferedWriter(new FileWriter(filename));
			
			vocabulary = MapUtil.sortByValue(vocabulary);
			
			for(String key : vocabulary.keySet())
			{
				bw.write(key + "\t" + vocabulary.get(key));
				bw.newLine();
			}
			
			bw.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.exit(-1);
		}
	}
	
	
	public static Map<String, Integer> shrinkVocabulary(Map<String, Integer> vocabulary)
	{
		vocabulary = MapUtil.sortByValue(vocabulary);
			
		Map<String, Integer> colVocab = new TreeMap<>();
		
		int index = 0;
		for(String key : vocabulary.keySet())
		{
			if(vocabulary.get(key) >= 75)
			{
				colVocab.put(key, index);
				index++;
			}
		}
		
		vocabulary.clear();
		
		return colVocab;
	}
}
