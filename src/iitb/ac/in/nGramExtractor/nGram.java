package iitb.ac.in.nGramExtractor;

public abstract class nGram 
{
	
	protected long ngramCount;
	public abstract void addWord(String... word);
	
	public abstract int getCount(String... word);

	public long getSize(){
		return ngramCount;
	}
	
}
