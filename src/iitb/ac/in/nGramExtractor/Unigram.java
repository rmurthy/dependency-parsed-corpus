package iitb.ac.in.nGramExtractor;

import iitb.ac.in.Utilities.MapUtil;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.Map;
import java.util.TreeMap;

public class Unigram extends nGram {
	public TreeMap<String,Integer> ngramWordCounter= new TreeMap<>();
	public Unigram() {
		ngramWordCounter = new TreeMap<String, Integer>();
	}

	@Override
	public void addWord(String... words) {
		if (words.length == 1) {
			if (ngramWordCounter.containsKey(words[0])) {
				ngramWordCounter.put(words[0], ngramWordCounter.get(words[0]) + 1);
			} else {
				ngramWordCounter.put(words[0], 1);
				ngramCount++;
			}
		}
	}
	
	@Override
	public int getCount(String... word) {
		if(word.length==1){
			if (ngramWordCounter.containsKey(word[0])) {
				return ngramWordCounter.get(word[0]);
			} 
		}
		return 0;
	}
	public void updateCount(String w,int count){
		ngramWordCounter.put(w, count);
	}
	public void dump(String filename){
		Map<String,Integer> temp=MapUtil.sortByValue(ngramWordCounter);
		try{
			BufferedWriter bw = new BufferedWriter(new FileWriter(filename));
			for(String key:temp.keySet()){
				if(temp.get(key)>2)
					bw.write(key+"\t"+temp.get(key)+"\n");
			}
			bw.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	public void compress()
	{
		Map<String,Integer> temp=MapUtil.sortByValue(ngramWordCounter);
		ngramWordCounter.clear();
		
		for(String key:temp.keySet())
		{
			if(temp.get(key) < 150 || temp.get(key) > 17000)
			{
				if (ngramWordCounter.containsKey("<UNK>")) 
				{
					ngramWordCounter.put("<UNK>", ngramWordCounter.get("<UNK>") + temp.get(key));
				}
				else 
				{
					ngramWordCounter.put("<UNK>", temp.get(key));
				}
			}
			else
			{
				if (ngramWordCounter.containsKey(key)) 
				{
					ngramWordCounter.put(key, ngramWordCounter.get(key) + temp.get(key));
				}
				else 
				{
					ngramWordCounter.put(key, temp.get(key));
				}
			}
		}
	}
}
