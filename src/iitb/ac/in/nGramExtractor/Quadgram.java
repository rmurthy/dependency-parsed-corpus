package iitb.ac.in.nGramExtractor;

import java.util.TreeMap;

public class Quadgram extends nGram {
	protected TreeMap<String, TreeMap<String, TreeMap<String, TreeMap<String, Integer>>>> ngramWordCounter;

	public Quadgram() {
		ngramWordCounter = new TreeMap<>();
	}

	@Override
	public void addWord(String... words) 
	{
		if(words.length==4){
			if (ngramWordCounter.containsKey(words[0])) {
				TreeMap<String,TreeMap<String, TreeMap<String, Integer>>> l1 = ngramWordCounter
						.get(words[0]);
				if (l1.containsKey(words[1])) {
					TreeMap<String, TreeMap<String,Integer>> l2 = l1.get(words[1]);
					if (l2.containsKey(words[2])) {
						TreeMap<String, Integer> l3 = l2.get(words[2]);
						if(l3.containsKey(words[3])){
							l3.put(words[3], l3.get(words[3]+1));
						}else{
							l3.put(words[3], 1);
							ngramCount++;
						}
					}else{
						TreeMap<String, Integer> t1 = new TreeMap<>();
						t1.put(words[3], 1);
						l2.put(words[2], t1);
						ngramCount++;
					}
				} else {
					TreeMap<String, TreeMap<String, Integer>> t1 = new TreeMap<>();
					TreeMap<String, Integer> t2 = new TreeMap<>();
					t2.put(words[3], 1);
					t1.put(words[2], t2);
					l1.put(words[1], t1);
					ngramCount++;
		
				}
			} else {
				TreeMap<String, TreeMap<String, TreeMap<String,Integer>>> t1 = new TreeMap<>();
				TreeMap<String, TreeMap<String, Integer>> t2 = new TreeMap<>();
				TreeMap<String, Integer> t3 = new TreeMap<>();
				t3.put(words[3], 1);
				t2.put(words[2], t3);
				t1.put(words[1], t2);
				ngramWordCounter.put(words[0], t1);
				ngramCount++;
			}
		}
	}

	@Override
	public int getCount(String... words) {
		if(words.length==4){
			if (ngramWordCounter.containsKey(words[0])) {
				if(ngramWordCounter.get(words[0]).containsKey(words[1])){
					if(ngramWordCounter.get(words[0]).get(words[1]).containsKey(words[2])){
						if(ngramWordCounter.get(words[0]).get(words[1]).get(words[2]).containsKey(words[3])){
							return ngramWordCounter.get(words[0]).get(words[1]).get(words[2]).get(words[3]);
						}
					}
				}
			}
		}
		return 0;
	}

}
