package iitb.ac.in.nGramExtractor;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.TreeMap;

public class Trigram extends nGram 
{
	protected TreeMap<String, TreeMap<String, TreeMap<String, Integer>>> ngramWordCounter;
	
	public Trigram() 
	{
		ngramWordCounter = new TreeMap<>();
	}

	@Override
	public void addWord(String... words) 
	{
		if (words.length == 3) 
		{
			if (ngramWordCounter.containsKey(words[0])) 
			{
				TreeMap<String, TreeMap<String, Integer>> l1 = ngramWordCounter
						.get(words[0]);
				if (l1.containsKey(words[1])) 
				{
					TreeMap<String, Integer> l2 = l1.get(words[1]);
					if (l2.containsKey(words[2])) 
					{
						l2.put(words[2], l2.get(words[2]) + 1);
					} 
					else 
					{
						l2.put(words[2], 1);
						ngramCount++;
					}
				} 
				else 
				{
					TreeMap<String, Integer> t1 = new TreeMap<>();
					t1.put(words[2], 1);
					l1.put(words[1], t1);
					ngramCount++;
				}
			} 
			else 
			{
				TreeMap<String, TreeMap<String, Integer>> t1 = new TreeMap<>();
				TreeMap<String, Integer> t2 = new TreeMap<>();
				t2.put(words[2], 1);
				t1.put(words[1], t2);
				ngramWordCounter.put(words[0], t1);
				ngramCount++;
			}
		}
	}

	@Override
	public int getCount(String... words) 
	{
		if(words.length==3)
		{
			if (ngramWordCounter.containsKey(words[0])) 
			{
				if(ngramWordCounter.get(words[0]).containsKey(words[1]))
				{
					if(ngramWordCounter.get(words[0]).get(words[1]).containsKey(words[2]))
					{
						return ngramWordCounter.get(words[0]).get(words[1]).get(words[2]);
					}
				}
			}
		}
		return 0;
	}
	
	public void saveTree(String filename)
	{
		try
		{
			BufferedWriter bw = new BufferedWriter(new FileWriter(filename));
			for(String key:ngramWordCounter.keySet())
			{
				for(String key1: ngramWordCounter.get(key).keySet())
				{
					for(String key2: ngramWordCounter.get(key).get(key1).keySet())
					{
						bw.write(key + " " +  key1  + " " + key2 +
								"\t"+ngramWordCounter.get(key).get(key1).get(key2) );
						bw.newLine();
					}
				}
					
			}
			bw.close();	
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}
