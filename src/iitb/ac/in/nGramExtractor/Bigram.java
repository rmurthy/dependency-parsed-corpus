package iitb.ac.in.nGramExtractor;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

public class Bigram extends nGram {
	public Map<String, TreeMap<String, Integer>> ngramWordCounter = null;

	public Bigram() {
		ngramWordCounter = new TreeMap<String, TreeMap<String, Integer>>();
	}

	@Override
	public void addWord(String... words) {
		if (words.length == 2) {
			if (ngramWordCounter.containsKey(words[0])) {
				TreeMap<String, Integer> val = ngramWordCounter.get(words[0]);
				if (val.containsKey(words[1])) {
					val.put(words[1], val.get(words[1]) + 1);
				} else {
					val.put(words[1], 1);
					ngramCount++;
				}
			} else {
				TreeMap<String, Integer> temp = new TreeMap<>();
				temp.put(words[1], 1);
				ngramWordCounter.put(words[0], temp);
				ngramCount++;
			}
		}
	}

	@Override
	public int getCount(String... words) {
		if (words.length == 2) {
			if (ngramWordCounter.containsKey(words[0])) {
				if (ngramWordCounter.get(words[0]).containsKey(words[1]))
					return ngramWordCounter.get(words[0]).get(words[1]);
			}
		}
		return 0;
	}

	public void dump(String filename)
	{
		try
		{
			BufferedWriter bw = new BufferedWriter(new FileWriter(filename));
			for(String key:ngramWordCounter.keySet())
			{
				for(String key1: ngramWordCounter.get(key).keySet())
				{
					bw.write(key + " " +  key1 +"\t"+ngramWordCounter.get(key).get(key1)+"\n");
				}
					
			}
			bw.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}	
	}
	
	public ArrayList<String> getFirstWord(String secondWord)
	{
		ArrayList<String> result = new ArrayList<String>();
		
		for(String key:ngramWordCounter.keySet())
		{
			for(String key1: ngramWordCounter.get(key).keySet())
			{
				if(key1.contentEquals(secondWord))
				{
					result.add(key);
					break;
				}
			}
		}
		return result;
	}
	
	public ArrayList<String> getBigram(String firstWord)
	{
		ArrayList<String> result = new ArrayList<String>();
		
		for(String key:ngramWordCounter.keySet())
		{
			if(key.contentEquals(firstWord))
			{
				for(String key1: ngramWordCounter.get(key).keySet())
				{
					result.add(key1);
				}
			}
		}
		return result;
	}
	
	public void saveTree(String filename)
	{
		try
		{
			BufferedWriter bw = new BufferedWriter(new FileWriter(filename));
			for(String key:ngramWordCounter.keySet())
			{
				for(String key1: ngramWordCounter.get(key).keySet())
				{
					bw.write(key + " " +  key1  + 
								"\t"+ngramWordCounter.get(key).get(key1) );
					bw.newLine();				
				}
					
			}
			bw.close();	
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void clear()
	{
		if(ngramWordCounter != null)
		{
			ngramWordCounter.clear();
		}
	}
}
