# README #

* This project constructs co-occurrence matrix using dependency relations ready to be used directly or after dimensionality reduction for Word Similarity Tasks

## Preliminaries ##
This program expects the corpus to contain single sentence per line and the text to be tokenized.

## Dependencies ##
This program uses [Stanford Dependency Parser](http://nlp.stanford.edu/software/lex-parser.shtml) for obtaining the dependency relation. Please add the corresponding jar files to the project.

### Main.java ###
This class contains the complete execution pipeline to the system. It reads in a corpus, constructs vocabulary, dependency parses a sentence and writes them in the syntax shown below.

This is an example sentence .
nsubj(sentence, This)cop(sentence, is)det(sentence, an)nn(sentence, example)root(ROOT,sentence)

The format is sentence followed by dependency relations in a new line followed by empty line to indicate end of that sentence.

## TreeCorpus.java ##
This class reads in the dependency parsed corpus in the above said syntax and constructs  bigrams where first word is the word under consideration and second word is related to it by one of the specified relationships.

## DependencyRelation.java ##
This class reads in the bigram file constructed above and constructs for every word an array containing it's corresponding words with which it is related along with frequency counts. The type of dependency relation needs to be specified in the configuration file.
Finally it writes a co-occurrence matrix for every word in sparse notation.

Eg: 
word1 1:123 4:20 200:2
word2 0:12 4:14 2000:23 2456:34

This sparse matrix can be used directly or fed into Matlab for dimensionality reduction and later used for Word Similarity Tasks.

### Running Using Jar file ###
dependencyParsedCorpus.jar can be used to run the entire pipeline. The arguments to be provided are 

```
#!shell
vocabulary inputFile outputFile lineNumber configurationFile

```

* vocabulary is the file in which the unigrams along with frequency has to be written
* inputFile is the link to the tokenized corpus
* outputFile is the file to which the sentence along with Dependency relations has to be written
* lineNumber is to resume the execution from a particular line number in the corpus
* configurationFile is used to select only a subset of dependency relations for considering the context words

**You need to manually link the Stanford Dependency Parser Libraries to run the complete pipeline**